<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Inventaris Apps</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/matrix-login.css" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
      <?php 
    if(isset($_GET['pesan'])){
        if($_GET['pesan']=="gagal"){
            echo "<div class='alert'>Username dan Password tidak sesuai !</div>";
        }
    }
    ?>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" action="cek_login.php" method="post">
                 <div class="control-group normal_text"> <h3><span>Login | </span><i class=" icon-linkedin-sign"></i>ventaris Apps</h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" name="username" placeholder="Username" required="">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" name="password" placeholder="Password" required="">
                        </div>
                    </div>
                </div>
                <div class="control-group normal_text" style="color:white;"><a href="peminjam/login.php"><b style="color:white;">Login User</b></a> |<a href="index.php"><b style="color:white;"> Login Petugas</b></a></div>
                <div class="control-group">
                <div class="form-actions">
                    <span class="pull-left"><a href="forget.php" class="flip-link btn btn-info" id="to-recover">Forgot password?</a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-success" value="LOGIN" /> </span>
                </div>
            </form>
          
        </div>
        
        <script src="js/jquery.min.js"></script>  
        <script src="js/matrix.login.js"></script> 
    </body>

</html>
