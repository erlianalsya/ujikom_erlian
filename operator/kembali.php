<?php
include 'header.php';
?>  
   <div class="container-fluid">
  <hr>
  <div class="row-fluid">
  <div class="span12">
             
          <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data table</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>

                                    <tr>
                                     
                                        <th>No</th>
                                        <th>ID Peminjaman</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Status Peminjam</th>
                                        <th>Nama Pegawai</th>
                                        <th>Aksi</th>
                                        
                                        </tr> 
                                    </thead>
                                    <tbody>
                                           <?php
                                           include '../koneksi.php';
                                           $no =1;                                           
                                             $data = mysqli_query($koneksi,"SELECT * from peminjaman join pegawai  ON peminjaman.id_pegawai=pegawai.id_pegawai where status_peminjaman='kembali' order by id_peminjaman desc");                                                         

                                           while($r = mysqli_fetch_array($data)){
                                              ?>

                                              <tr>
                                                  <th scope="row"><?php echo $no++;?></th>
                                                  <td><?php echo $r['id_peminjaman']; ?></td>
                                                  <td><?php echo $r['tgl_pinjam']; ?></td>
                                                  <td><?php echo $r['tgl_kembali']; ?></td>
                                                  <td><?php echo $r['status_peminjaman']; ?></td>
                                                  <td><?php echo $r['nama_pegawai']; ?></td>
                                                  <td> <a href='detail_kembali.php?id_peminjaman=<?php echo $r['id_peminjaman'];?>' type="button" class="btn btn-info icon icon-eye ">View</a></td> 
                                              </tr>
                                              <?php 
                                          }
                                          ?>
                                        </tbody>
                              
                          </table>
                      </div>
                 </div>
                 </div>
                 </div>
                 </div>

                      
      </div><!--/row-->
  </div><!--/main-content end--> 



      </div>
      </div>
      <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
  </div>
  <!--\\\\\\\ inner end\\\\\\-->
</div>
</div>
</div>
</div>
<!--\\\\\\\ wrapper end\\\\\\-->
<!-- Modal -->

<!-- /sidebar chats -->   

<?php include "footer.php"; ?>
