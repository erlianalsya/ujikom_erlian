  
 <?php
 include "header.php";
 ?>

<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Tambah Data Barang</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="simpan_brg.php" method="post" class="form-horizontal">
          <?php
                            include "../koneksi.php";
                            $id_peminjaman=$_GET['id_peminjaman'];
                            $result =mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM peminjaman where id_peminjaman='$id_peminjaman' ")); 
                            ?>

            <div class="control-group">
              <label class="control-label">ID Peminjaman :</label>
              <div class="controls">
                <input type="text" name="id_peminjaman" class="span11" placeholder="Masukan Nama Jenis" value="<?php echo $result['id_peminjaman']; ?>" readonly>
              </div>
            </div>
            
            <div class="form-group">
          <label class="control-label">Nama Barang :</label>
          <div class="controls">
          <input type="hidden"  name="id_inventaris" class="form-control">
          <select class="form-control"  name="id_inventaris"  required="">
              <option value="">--- Silahkan Cari ---</option>

              <?php
              include_once "../koneksi.php";
              $tampil=mysqli_query($koneksi,"SELECT * FROM inventaris ORDER BY id_inventaris");
              while($r=mysqli_fetch_array($tampil)){
                ?>
                <option value="<?php echo $r['id_inventaris']?>"> <?php echo $r['nama'] ?> | <?php echo "(stock : ".$r['jumlah'].")" ?></option>
                <?php
            }
            ?>

        </select>
        </div>
        </div>
            <div class="control-group">
              <label class="control-label">Jumlah :</label>
              <div class="controls">
                <input type="number" name="jumlah_pinjam" class="span11" placeholder="" value="<?php echo $result['jumlah_pinjam']; ?>">
              </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
             <a href="pinjam.php" type="submit" class="btn btn-danger">Cancel</a>
            </div>
          </form>
        </div>
      </div>
      </div>
      </div>
    

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data table</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>

                                    <tr>
                                     
                                        <th>No</th>
                                        <th>ID Barang</th>
                                        <th>Jumlah</th>
                                        <th>ID Peminjam</th>
                                        <th>Status Peminjam</th> 
                                        
                                        </tr> 
                                    </thead>
                                    <tbody>
                                           <?php
                                           include '../koneksi.php';
                                           $no =1;
                                           
                                             $data = mysqli_query($koneksi," select * from detail_pinjam where id_peminjaman='$_GET[id_peminjaman]' order by id_peminjaman desc");
                                         
                                          

                                           while($r = mysqli_fetch_array($data)){
                                              ?>

                                              <tr>
                                                  <th scope="row"><?php echo $no++;?></th>
                                                  <td><?php echo $r['id_inventaris']; ?></td>
                                                  <td><?php echo $r['jumlah_pinjam']; ?></td>
                                                  <td><?php echo $r['id_peminjaman']; ?></td>
                                                  <td><?php echo $r['status_pinjam']; ?></td> 
                                                 
                                              </tr>
                                              <?php 
                                          }
                                          ?>
                                        </tbody>
                              
                          </table>
                      </div>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>

                      
   
  </div><!--/main-content end--> 

                                   
<?php include "footer.php"; ?>
