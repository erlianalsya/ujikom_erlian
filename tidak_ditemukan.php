<?php
include 'header.php';
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="<?php echo base_url() ?>">Admin</a>
    </li>
  <li class="breadcrumb-item active">Halaman Tidak Ditemukan</li>
</ol>

<div class="row">
    <div class="col-12">
    	<h1 class="animated bounceInLeft">Halaman Tidak Ditemukan</h1>
        <p class="animated bounceInRight">Halaman yang diminta mungkin tidak ada, atau Anda tidak memiliki akses untuk halaman tersebut.</p>
    </div>
</div>
<?php include'footer.php';?>