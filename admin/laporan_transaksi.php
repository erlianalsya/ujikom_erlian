<?php
include "header.php";
?>  



    <div class="container-fluid">
    <hr>
     <button type="button"  data-toggle="modal" data-target="#export2" class="btn btn-success"><i class="icon icon-file" aria-hidden="true" style="color: white;"> Export to Excel</i></a></button>
     <button type="button"  data-toggle="modal" data-target="#export" class="btn btn-danger"><i class="icon icon-print" aria-hidden="true" style="color: white;"> Cetak PDF</i></a></button>
         <div class="row-fluid">
      <div class="span12">
         <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data laporan Transaksi</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
              <tr>
                                            <th>No</th>
                                            <th>ID Barang</th> 
                                            <th>ID Peminjaman</th>
                                            <th>Nama Pegawai</th>
                                            <th>Nama Barang</th>
                                            <th>Nama Ruang</th>
                                            <th>Jumlah</th>
                                            <th>Status Peminjaman</th>
                                            <th>Tanggal Pinjam</th>
                                            <th>Tanggal Kembali</th>
                                         
                                      
                                        </tr> 
                                    </thead>
                                    <tbody>
                                  <?php
                          include "../koneksi.php";
                          $no=1; 
                          $query_mysqli = mysqli_query ($koneksi,"SELECT * from peminjam ORDER BY id_peminjaman DESC");
                          while($data = mysqli_fetch_array($query_mysqli)){
                        ?>
                                       <tr>
                        <td><?php echo $no++ ?></td> 
                        <td><?php echo $data['id_inventaris'] ?></td>
                        <td><?php echo $data['id_peminjaman'] ?></td>
                        <td><?php echo $data['nama_pegawai'] ?></td>
                        <td><?php echo $data['nama'] ?></td>
                        <td><?php echo $data['nama_ruang'] ?></td>
                        <td><?php echo $data['jumlah'] ?></td>
                        <td><?php echo $data['status_peminjaman'] ?></td>
                        <td ><?php echo $data['tgl_pinjam'] ?></td>
                        <td><?php echo $data['tgl_kembali'] ?></td>
 
                      
                    </tr>
                                        <?php
                                    }
                                    ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Transaksi</h4>
</div>
<div class="modal-body">
<form action="pdfall_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_kembali" class="btn btn-inverse btn-sm" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_all.php" target="_blank" class="btn btn-inverse btn-sm" >Cetak Semua</a>
</div>
</div>
</div>
</div>


<div id="export2" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Transaksi </h4>
</div>
<div class="modal-body">
<form action="excelall_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_pinjam" class="btn btn-inverse btn-sm" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_all.php" target="_blank" class="btn btn-inverse btn-sm" >Cetak Semua</a>
</div>
</div>
</div>
</div>


                            

<?php 
include "footer.php";
 ?>
