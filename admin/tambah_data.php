  
 <?php
 include "header.php";
 ?>

<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Tambah Data Barang</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="simpan_data.php" method="post" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Nama Barang :</label>
              <div class="controls">
                <input type="text" name="nama" class="span11" placeholder="masukan nama barang" required="">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Kondisi :</label>
              <div class="controls">
                <input type="text" class="span11" name="kondisi" placeholder="masukan kondisi" required="">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Spesifikasi</label>
              <div class="controls">
                <input type="text"  class="span11" name="spesifikasi" placeholder="masukan spesifikasi"  required="">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Keterangan</label>
              <div class="controls">
                <input type="text" class="span11" name="keterangan" placeholder="masukan keterangan" required="">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Jumlah</label>
              <div class="controls">
                <input type="number" class="span11" name="jumlah" placeholder="masukan jumlah" required="">
              </div>
            </div>
            <div class="control-group">
             <?php
  include_once "../koneksi.php";
  $result = mysqli_query($koneksi,"select * from jenis order by nama_jenis asc ");
  $jsArray = "var nama_jenis = new Array();\n";
  ?>
              <label class="control-label">Nama Jenis</label>
              <div class="controls">
               <select class="span11" id="source" name="id_jenis"  onchange="changeValue(this.value)" required="">
                                            <option value="AK"> ..Pilih Jenis..
                                             <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "nama_jenis['". $row['nama_jenis']. "'] = {satu:'" . addslashes($row['nama_jenis']) . "'};\n";
  }
  ?> </option>
                                         
                                          </select>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Tanggal Register</label>
              <div class="controls">
                <input type="date" class="span11" name="tgl_register" value="<?php 
                                            $tgl = Date('Y-m-d');
                                            echo $tgl;
                                            ?>" readonly>
              </div>
            </div>


            <div class="control-group">
             <?php
  include_once "../koneksi.php";
  $result = mysqli_query($koneksi,"select * from ruang order by nama_ruang asc ");
  $jsArray = "var nama_ruang = new Array();\n";
  ?>
              <label class="control-label">Nama Ruang</label>
              <div class="controls">
                <select class="span11" id="source" name="nama_ruang"  onchange="changeValue(this.value)" required="">
                                            <option value="AK"> ..Pilih Jenis..
                                             <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "nama_ruang['". $row['nama_ruang']. "'] = {satu:'" . addslashes($row['nama_ruang']) . "'};\n";
  }
  ?> </option>
                                           
                                          </select>
              </div>
            </div>

             <div class="control-group">
              <label class="control-label">Kode Inventaris</label>
               <?php
                                  
                                $koneksi=mysqli_connect("localhost","root","","inventaris_smk");
                        
                                    $cari_kd = mysqli_query($koneksi,"SELECT kode_inventaris from Inventaris order by id_inventaris desc");
                                    $data = mysqli_fetch_array($cari_kd)
                                    ?>

                                     <div class="controls">

                                    <input type="text" name="kode_inventaris"  value="<?php echo substr($data['kode_inventaris'],4,1)+1 ?>" class="span11" placeholder="Masukan Kode Inventaris Anda" readonly>
  
              </div>
              </div>
           

              <div class="control-group">
             <?php
  include_once "../koneksi.php";
  $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
  $jsArray = "var id_petugas = new Array();\n";
  ?>
              <label class="control-label">Nama Petugas</label>
              <div class="controls">
                <select class="span11" id="source" name="nama_petugas"  onchange="changeValue(this.value)" required="">
                                            <option value="AK"> ..Pilih Petugas..
                                             <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "nama_petugas['". $row['nama_petugas']. "'] = {satu:'" . addslashes($row['nama_petugas']) . "'};\n";
  }
  ?> </option>

  </select>
                                           
                      
              </div>
            </div>

             <div class="control-group">
              <label class="control-label">Sumber</label>
              <div class="controls">
                <input type="text" class="span11" name="sumber" placeholder="masukan sumber" required="">
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
              <button type="submit" class="btn btn-danger">Cancel</button>
            </div>
          </form>
        </div>
      </div>
      </div>
      </div>
      </div>
        </div>

                                   
<?php include "footer.php"; ?>
