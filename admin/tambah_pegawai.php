  
 <?php
 include "header.php";
 ?>

<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Tambah Data Pegawai</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="simpan_pegawai.php" method="post" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Nama Pegawai :</label>
              <div class="controls">
                <input type="text" name="nama_pegawai" class="span11" placeholder="Masukan nama anda" required="">
              </div>
            </div>

             <div class="control-group">
              <label class="control-label">Username :</label>
              <div class="controls">
                <input type="text" name="username" class="span11" placeholder="Masukan Username anda" required="">
              </div>
            </div>

             <div class="control-group">
              <label class="control-label">Password :</label>
              <div class="controls">
                <input type="text" name="password" class="span11" placeholder="Masukan Password anda" required="">
              </div>
            </div>

             <div class="control-group">
              <label class="control-label">Email :</label>
              <div class="controls">
                <input type="text" name="email" class="span11" placeholder="Masukan email anda" required="">
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">NIP :</label>
              <div class="controls">
                <input type="text" name="nip" class="span11" placeholder="Masukan nip anda" required="">
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Alamat :</label>
              <div class="controls">
                <input type="text" name="alamat" class="span11" placeholder="Masukan alamat anda" required="">
              </div>
            </div>


            <div class="form-actions">
              <button type="submit" name="submit" class="btn btn-success">Save</button>
              <button type="submit" class="btn btn-danger">Cancel</button>
            </div>
          </form>
        </div>
      </div>
      </div>
      </div>
      </div>
        </div>

                                   
<?php include "footer.php"; ?>
