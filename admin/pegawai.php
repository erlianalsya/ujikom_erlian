<?php
include "header.php";
?>  



         <div class="container-fluid">
    <hr>
     <a href="tambah_pegawai.php" class="btn btn-danger icon icon-plus">&nbsp;Tambah Data</a> 
         <div class="row-fluid">
      <div class="span12">
         <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data table</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
              <tr>
                                     
                                        <th>No</th>
                                        <th>Nama Pegawai</th>
                                        <th>Username</th>
                                        <th>Password</th>
                                        <th>Email</th>
                                        <th>NIP</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                   <?php
                                    include "../koneksi.php";
                                    $no=1;
                                    $select=mysqli_query($koneksi, "select * from Pegawai order by id_pegawai desc ");
                                    while($data=mysqli_fetch_array($select))
                                    {
                                    ?>
                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_pegawai'];?></td>
                                        <td><?php echo $data['username'];?></td>
                                        <td><?php echo $data['password'];?></td>
                                        <td><?php echo $data['email'];?></td>
                                        <td><?php echo $data['nip']; ?></td>
                                        <td><?php echo $data['alamat']; ?></td>
                                        
                                            <td>
                                            <a class="btn btn outline btn-primary icon icon-edit" href="edit_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>"></a> |
                                            <a class="btn btn outline btn-danger icon icon-trash" href="hapus_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>"></a></td>   
                                        </tr>
                                        <?php
                                    }
                                    ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

                            

<?php 
include "footer.php";
 ?>
