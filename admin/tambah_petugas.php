  
 <?php
 include "header.php";
 ?>

<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Tambah Data Petugas</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="simpan_petugas.php" method="post" class="form-horizontal">
          
            <div class="control-group">
              <label class="control-label">Username :</label>
              <div class="controls">
                <input type="text" name="username" class="span11" placeholder="Masukan username anda" required="">
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Email :</label>
              <div class="controls">
                <input type="text" name="email" class="span11" placeholder="Masukan email anda" required="">
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Password :</label>
              <div class="controls">
                <input type="text" name="password" class="span11" placeholder="Masukan password anda" required="">
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Nama Petugas :</label>
              <div class="controls">
                <input type="text" name="nama_petugas" class="span11" placeholder="Masukan Nama anda" required="">
              </div>
            </div>

              <div class="control-group">
             <?php
  include_once "../koneksi.php";
  $result = mysqli_query($koneksi,"select * from level order by id_level asc ");
  $jsArray = "var id_level = new Array();\n";
  ?>
              <label class="control-label">Level</label>
              <div class="controls">
                <select class="span11" id="source" name="id_level"  onchange="changeValue(this.value)" required="">
                                            <option value="AK"> ..Pilih Level..
                                             <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "level['". $row['level']. "'] = {satu:'" . addslashes($row['level']) . "'};\n";
  }
  ?> </option>

  </select>
                                           
                      
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Baned :</label>
              <div class="controls">
                <input type="text" name="baned" class="span11" placeholder="Masukan Baned anda" required="">
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">LoginTime :</label>
              <div class="controls">
                <input type="text" name="logintime" class="span11" placeholder="Masukan LoginTime anda" required="">
              </div>
            </div>

            <div class="form-actions">
              <button type="submit" name="submit" class="btn btn-success">Save</button>
              <button type="submit" name="cancel" class="btn btn-danger">Cancel</button>
            </div>
          </form>
        </div>
      </div>
      </div>
      </div>
      </div>
        </div>

                                   
<?php include "footer.php"; ?>
