<?php
include "header.php";
?>  



    <div class="container-fluid">
    <hr>
     <button type="button"  data-toggle="modal" data-target="#export2" class="btn btn-success"><i class="icon icon-file" aria-hidden="true" style="color: white;"> Export to Excel</i></a></button>
     <button type="button"  data-toggle="modal" data-target="#export" class="btn btn-danger"><i class="icon icon-print" aria-hidden="true" style="color: white;"> Cetak PDF</i></a></button>
         <div class="row-fluid">
      <div class="span12">
         <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data laporan Barang</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
              <tr>
                                      <th>No</th>
                                        <th>Nama</th>
                                        <th>Kondisik</th>
                                        <th>Spesifikasi</th>
                                        <th>Keterangan</th>
                                        <th>Jumlah</th>
                                        <th>Jenis Barang</th>
                                        <th>Tanggal Register</th>
                                        <th>Ruangan</th>
                                        <th>Kode Inventaris</th>
                                        <th>Nama Petugas</th>
                                        <th>Sumber</th>
                                      
                                        </tr> 
                                    </thead>
                                    <tbody>
                                  <?php
                                    include "../koneksi.php";
                                    $no=1;
                                    $select= mysqli_query($koneksi,"SELECT `inventaris`.id_inventaris,`inventaris`.kode_inventaris as kode_inventaris,`inventaris`.nama as nama,`inventaris`.kondisi as kondisi,`inventaris`.spesifikasi as spesifikasi,`inventaris`.keterangan as keterangan,`inventaris`.jumlah as jumlah,`inventaris`.tgl_register as tgl_register,`inventaris`.sumber as sumber,`jenis`.nama_jenis as nama_jenis,`ruang`.nama_ruang as nama_ruang,`petugas`.nama_petugas as nama_petugas FROM `inventaris` JOIN `jenis` ON `inventaris`.id_jenis=`jenis`.id_jenis JOIN `petugas` ON `inventaris`.id_petugas=`petugas`.id_petugas JOIN `ruang` ON `inventaris`.id_ruang=`ruang`.id_ruang order by `inventaris`.id_inventaris desc ");
                                    while($data=mysqli_fetch_array($select))
                                    {
                                    ?> 

                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama']; ?></td>
                                        <td><?php echo $data['kondisi']; ?></td>
                                        <td><?php echo $data['spesifikasi']; ?></td>
                                        <td><?php echo $data['keterangan']; ?></td>
                                        <td><?php echo $data['jumlah']; ?></td>
                                        <td><?php echo $data['nama_jenis']; ?></td>
                                        <td><?php echo $data['tgl_register']; ?></td>
                                        <td><?php echo $data['nama_ruang']; ?></td>
                                        <td><?php echo $data['kode_inventaris']; ?></td>
                                        <td><?php echo $data['nama_petugas']; ?></td>
                                        <td><?php echo $data['sumber']; ?></td>
                                         
                                        

                                        </tr>
                                        <?php
                                    }
                                    ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Inventaris </h4>
</div>
<div class="modal-body">
<form action="cetak_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-success" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="ctk_inventaris.php" target="_blank" class="btn btn-succes" >Cetak Semua</a>
</div>
</div>
</div>
</div>


<div id="export2" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Inventaris </h4>
</div>
<div class="modal-body">
<form action="ex_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-success" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export.php" target="_blank" class="btn btn-success" >Cetak Semua</a>
</div>
</div>
</div>
</div>


                            

<?php 
include "footer.php";
 ?>
