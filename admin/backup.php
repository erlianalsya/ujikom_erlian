<?php
$connect = new PDO("mysql:host=localhost;dbname=inventaris_smk", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $connect->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
 $output = '';
 foreach($_POST["table"] as $table)
 {
  $show_table_query = "SHOW CREATE TABLE " . $table . "";
  $statement = $connect->prepare($show_table_query);
  $statement->execute();
  $show_table_result = $statement->fetchAll();

  foreach($show_table_result as $show_table_row)
  {
   $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
  }
  $select_query = "SELECT * FROM " . $table . "";
  $statement = $connect->prepare($select_query);
  $statement->execute();
  $total_row = $statement->rowCount();

  for($count=0; $count<$total_row; $count++)
  {
   $single_result = $statement->fetch(PDO::FETCH_ASSOC);
   $table_column_array = array_keys($single_result);
   $table_value_array = array_values($single_result);
   $output .= "\nINSERT INTO $table (";
   $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
   $output .= "'" . implode("','", $table_value_array) . "');\n";
  }
 }
 $file_name = 'inventaris_smk_' . date('y-m-d') . '.sql';
 $file_handle = fopen($file_name, 'w+');
 fwrite($file_handle, $output);
 fclose($file_handle);
 header('Content-Description: File Transfer');
 header('Content-Type: application/octet-stream');
 header('Content-Disposition: attachment; filename=' . basename($file_name));
 header('Content-Transfer-Encoding: binary');
 header('Expires: 0');
 header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_name));
    ob_clean();
    flush();
    readfile($file_name);
    unlink($file_name);
}

?>
  <?php
include "header.php";
?>  



    <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
         <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Backup Database</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content">
            <table class="table data-table">
              <thead>
              <tr>
                      
      <form method="post" id="export_form">
     <h4>Pilih Tabel Untuk Export</h4>
    <?php
    foreach($result as $table)
    {
    ?>

    
    
     <div class="checkbox">
      <label><input  type="checkbox" checked  class="checkbox_table" name="table[]" value="<?php echo $table["Tables_in_inventaris_smk"]; ?>" /> <?php echo $table["Tables_in_inventaris_smk"]; ?></label>
     </div>
    <?php
    }
    ?>
     <div class="form-group">
      <input type="submit" name="submit" id="submit" class="btn btn-info" value="Export" />
     </div>
      <div class="box-content">
            
     
                </form>
                </tr> 
                </thead>
                              
                                    
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

                            

<?php 
include "footer.php";
 ?>
