<?php
include "header.php";
?>  



    <div class="container-fluid">
    <hr>
     <button type="button"  data-toggle="modal" data-target="#export2" class="btn btn-success"><i class="icon icon-file" aria-hidden="true" style="color: white;"> Export to Excel</i></a></button>
     <button type="button"  data-toggle="modal" data-target="#export" class="btn btn-danger"><i class="icon icon-print" aria-hidden="true" style="color: white;"> Cetak PDF</i></a></button>
         <div class="row-fluid">
      <div class="span12">
         <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data laporan Barang</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
              <tr>
                                            <th>No</th>
                                            <th>Nama</th> 
                                            <th>Tanggal Pinjam</th>
                                            <th>Tanggal Kembali</th>
                                            <th>Status</th> 
                                      
                                        </tr> 
                                    </thead>
                                    <tbody>
                                  <?php
                          include "../koneksi.php";
                          $no=1; 
                          $query_mysqli = mysqli_query ($koneksi,"SELECT * from peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai=pegawai.id_pegawai where status_peminjaman='Pinjam' ORDER BY id_peminjaman DESC");
                          while($data = mysqli_fetch_array($query_mysqli)){
                        ?>
                                       <tr>
                        <td><?php echo $no++ ?></td> 
                        <td><?php echo $data['nama_pegawai'] ?></td>
                        <td align="center"><?php echo $data['tgl_pinjam'] ?></td>
                        <td><?php echo $data['tgl_kembali'] ?></td>
                        <td align="center"><?php echo $data['status_peminjaman'] ?></td>  
                      
                    </tr>
                                        <?php
                                    }
                                    ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Pinjam </h4>
</div>
<div class="modal-body">
<form action="pdfpinjam_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_pinjam" class="btn btn-success" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_pinjam.php" target="_blank" class="btn btn-success" >Cetak Semua</a>
</div>
</div>
</div>
</div>


<div id="export2" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Peminjaman </h4>
</div>
<div class="modal-body">
<form action="excelpinjam_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_pinjam" class="btn btn-success" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_pinjam.php" target="_blank" class="btn btn-success" >Cetak Semua</a>
</div>
</div>
</div>
</div>


                            

<?php 
include "footer.php";
 ?>
