<?php
include 'header.php';
?>  
   <div class="container-fluid">
  <hr>
  <div class="row-fluid">
  <div class="span12">
            
  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal" ><i class="icon icon-plus" aria-hidden="true" style="color: white;"> Pinjam</i></a></button>
          <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data table</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>

                                    <tr>
                                     
                                        <th>No</th>
                                        <th>ID Peminjaman</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Status Peminjam</th>
                                        <th>Nama Pegawai</th>
                                        <th>Aksi</th>
                                        
                                        </tr> 
                                    </thead>
                                    <tbody>
                                           <?php
                                           include '../koneksi.php';
                                           $no =1;                                           
                                             $data = mysqli_query($koneksi," SELECT  * from peminjaman JOIN pegawai on peminjaman.id_pegawai=pegawai.id_pegawai where status_peminjaman='pinjam' order by id_peminjaman desc");                                                         

                                           while($r = mysqli_fetch_array($data)){
                                              ?>

                                              <tr>
                                                  <th scope="row"><?php echo $no++;?></th>
                                                  <td><?php echo $r['id_peminjaman']; ?></td>
                                                  <td><?php echo $r['tgl_pinjam']; ?></td>
                                                  <td><?php echo $r['tgl_kembali']; ?></td>
                                                  <td><?php echo $r['status_peminjaman']; ?></td>
                                                  <td><?php echo $r['nama_pegawai']; ?></td>
                                                  <td> <a href='detail_kembali.php?id_peminjaman=<?php echo $r['id_peminjaman'];?>' type="button" class="btn btn-info icon icon-eye ">View</a></td> 
                                              </tr>
                                              <?php 
                                          }
                                          ?>
                                        </tbody>
                              
                          </table>
                      </div>
                 </div>
                 </div>
                 </div>
                 </div>

                      
      </div><!--/row-->
  </div><!--/main-content end--> 






<div id="myModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">peminjam</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="simpan_pinjam1.php">
        <div class="form-group">
          <label>Tanggal Pinjam</label>
          <input type="text" name="tgl_pinjam" class="form-control" value="<?= date('1,Y-m-d h:i:s a'); ?>" readonly>
          <input type="hidden" name="tgl_kembali" class="form-control" value="" readonly>
           <input type="hidden" name="status_peminjaman" class="form-control" value="pinjam" readonly>
        </div>
       
        <div class="form-group">
          <label>Peminjam</label>
          <select class="form-control"  name="id_pegawai"  required="">
              <option value="">--- Silahkan Cari ---</option>

              <?php
              include_once "../koneksi.php";
              $tampil=mysqli_query($koneksi,"SELECT * FROM pegawai ORDER BY id_pegawai desc");
              while($r=mysqli_fetch_array($tampil)){
                ?>
                <option value="<?php echo $r['id_pegawai']?>"> <?php echo $r['nama_pegawai'] ?></option>
                <?php
            }
            ?>

        </select>
        </div>
       <div class="form-actions">
              <button type="submit" class="btn btn-success">Tambah</button>
             <a href="pinjam.php" type="submit" class="btn btn-danger">Cancel</a>
            </div>
        </form>
      </div>
    </div>
  </div>
                                    <!-- modal -->
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>      
      



      </div>
      </div>
      <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
  </div>
  <!--\\\\\\\ inner end\\\\\\-->
</div>
</div>
</div>
</div>
<!--\\\\\\\ wrapper end\\\\\\-->
<!-- Modal -->

<!-- /sidebar chats -->   

<?php include "footer.php"; ?>
