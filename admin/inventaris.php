<?php
include "header.php";
?>  
<div class="container-fluid">
  <hr>
  <a href="tambah_data.php" class="btn btn-danger icon icon-plus">&nbsp;Tambah Data</a> 
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
          <h5>Data table</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="table-responsive">
            <table class="table table-bordered data-table">
              <thead>
              <tr>
                                     
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kondisik</th>
                                        <th>Spesifikasi</th>
                                        <th>Keterangan</th>
                                        <th>Jumlah</th>
                                        <th>Jenis Barang</th>
                                        <th>Tanggal Register</th>
                                        <th>Ruangan</th>
                                        <th>Kode Inventaris</th>
                                        <th>Nama Petugas</th>
                                        <th>Sumber</th>
                                        <th>Aksi</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                    <?php
                                    include "../koneksi.php";
                                    $no=1;
                                    $select=mysqli_query($koneksi,"SELECT `inventaris`.id_inventaris,`inventaris`.kode_inventaris as kode_inventaris,`inventaris`.nama as nama,`inventaris`.kondisi as kondisi,`inventaris`.spesifikasi as spesifikasi,`inventaris`.keterangan as keterangan,`inventaris`.jumlah as jumlah,`inventaris`.tgl_register as tgl_register,`inventaris`.sumber as sumber,`jenis`.nama_jenis as nama_jenis,`ruang`.nama_ruang as nama_ruang,`petugas`.nama_petugas as nama_petugas FROM `inventaris` JOIN `jenis` ON `inventaris`.id_jenis=`jenis`.id_jenis JOIN `petugas` ON `inventaris`.id_petugas=`petugas`.id_petugas JOIN `ruang` ON `inventaris`.id_ruang=`ruang`.id_ruang order by `inventaris`.id_inventaris desc ");
                                    while($data=mysqli_fetch_array($select))
                                    {
                                    ?> 
                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama']; ?></td>
                                        <td><?php echo $data['kondisi']; ?></td>
                                        <td><?php echo $data['spesifikasi']; ?></td>
                                        <td><?php echo $data['keterangan']; ?></td>
                                        <td><?php echo $data['jumlah']; ?></td>
                                        <td><?php echo $data['nama_jenis']; ?></td>
                                        <td><?php echo $data['tgl_register']; ?></td>
                                        <td><?php echo $data['nama_ruang']; ?></td>
                                        <td><?php echo $data['kode_inventaris']; ?></td>
                                        <td><?php echo $data['nama_petugas']; ?></td>
                                        <td><?php echo $data['sumber']; ?></td>
                                         
                                         <td>
                                            <a class="btn btn outline btn-primary icon icon-edit" href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"></a> |
                                            <a class="btn btn outline btn-danger icon icon-trash" href="hapus.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"></a></td> 

                                        </tr>
                                        <?php
                                    }
                                    ?>
              </tbody>
            </table>



            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

                            

<?php 
include "footer.php";
 ?>
