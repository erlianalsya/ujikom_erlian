<?php 
session_start();
include '../koneksi.php';
if (!isset($_SESSION['username'])){
    header("location:../index.php?pesan=belum_login");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Inventaris Apps</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="../css/bootstrap.min.css" />
<link rel="stylesheet" href="../css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="../css/fullcalendar.css" />
<link rel="stylesheet" href="../css/matrix-style.css" />
<link rel="stylesheet" href="../css/matrix-media.css" />
<link href="../font-awesome/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/jquery.gritter.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<!--Header-part-->

<div id="header">
  <h3><a href="dashboard.html"><i class=" icon-linkedin-sign"></i>ventaris Apps</a></h3>
</div>

<!--close-Header-part--> 


<!--top-Header-menu--> 
<div id="user-nav" class="navbar navbar-inverse">
<?php
        $use=$_SESSION['username'];
        $fo=mysqli_query($koneksi, "select nama_petugas from petugas where username='$use'");
        while($f=mysqli_fetch_array($fo)){
        ?>
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text"><?php echo $f['nama_petugas'];?></span> <?php } ?>
    <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="../index.php"><i class="icon-key"></i> Log Out</a></li>
      </ul>
    </li>
  </ul>
</div>

<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>

  <ul>
    <li class="active"><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
      <li class="submenu"> <a href="#"><i class="icon icon-th"></i> <span>Inventaris</span> <span class="label label-important"></span></a>
      <ul>
        <li><a href="inventaris.php">Data Barang</a></li>
        <li><a href="jenis_inventaris.php">Jenis Barang</a></li>
        <li><a href="ruang_inventaris.php">Data Ruangan</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Transaksi</span> <span class="label label-important"></span></a>
      <ul>
        <li><a href="pinjam.php">Peminjaman</a></li>
        <li><a href="kembali.php">Pengembalian</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-group"></i> <span>User</span> <span class="label label-important"></span></a>
      <ul>
        <li><a href="petugas.php">Data Petugas</a></li>
        <li><a href="pegawai.php">Data Pegawai</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-folder-open"></i> <span>Laporan</span> <span class="label label-important"></span></a>
      <ul>
        <li><a href="laporan_barang.php">Laporan Barang</a></li>
        <li><a href="laporan_pinjam.php">Laporan Peminjaman</a></li>
        <li><a href="laporan_kembali.php">Laporan Pengembalian</a></li>
        <li><a href="laporan_Transaksi.php">Semua Data Transaksi</a></li>
      </ul>
    </li>

    <li><a href="backup.php"><i class="icon icon-qrcode"></i> <span>Backup Database</span></a></li>
  </ul>
</div> 
<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
