  
 <?php
 include "header.php";
 ?>

<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Tambah Data Barang</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="simpan_ruang.php" method="post" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Nama Ruang :</label>
              <div class="controls">
                <input type="text" name="nama_ruang" class="span11" placeholder="Masukan Nama Ruang" />
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Kode Ruang :</label>
               <?php
                                  
                                $koneksi=mysqli_connect("localhost","root","","inventaris_smk");
                        
                                    $cari_kd = mysqli_query($koneksi,"SELECT kode_ruang from ruang order by id_ruang desc");
                                    $data = mysqli_fetch_array($cari_kd)
                                    ?>

                                     <div class="controls">

                                    <input type="text" name="kode_ruang"  value="<?php echo substr($data['kode_ruang'],4,1)+1 ?>" class="span11" placeholder="Masukan Kode Ruang Anda" readonly>
  
              </div>
              </div>

            <div class="control-group">
              <label class="control-label">Keterangan :</label>
              <div class="controls">
                <input type="text"  class="span11" name="keterangan" placeholder="Masukan Keterangan"  required="">
              </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
              <button type="submit" class="btn btn-danger">Cancel</button>
            </div>
          </form>
        </div>
      </div>
      </div>
      </div>
      </div>
        </div>

                                   
<?php include "footer.php"; ?>
