<?php
include "header.php";
?>  



         <div class="container-fluid">
    <hr>
     <a href="tambah_jenis.php" class="btn btn-danger icon icon-plus">&nbsp;Tambah Data</a> 
         <div class="row-fluid">
      <div class="span12">
         <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data table</h5>
          </div>
          <div class="table-responsive">
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
              <tr>
                                     
                                        <th>No</th>
                                        <th>Nama Jenis</th>
                                        <th>Kode Jenis</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                    <?php
                                    include "../koneksi.php";
                                     $no=1;
                                    $select=mysqli_query($koneksi, "select * from jenis  order by id_jenis desc");
                                    while($data=mysqli_fetch_array($select))
                                    {
                                    ?> 
                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_jenis']; ?></td>
                                        <td><?php echo $data['kode_jenis']; ?></td>
                                        <td><?php echo $data['keterangan']; ?></td>
                                        
                                            <td>
                                            <a class="btn btn outline btn-primary icon icon-edit" href="edit_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>"></a> |
                                            <a class="btn btn outline btn-danger icon icon-trash" href="hapus_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>"></a></td>    
                                        </tr>
                                        <?php
                                    }
                                    ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

                            

<?php 
include "footer.php";
 ?>
